# simple-webapp

## Launch Server using Vagrant with Ansible + Docker

### Prerequisite
First, make sure you have the Vagrant, Ansible and Docker toolkit
Review installation instructions

1.[Vagrant](https://www.vagrantup.com/downloads.html)

2.[Ansible](http://docs.ansible.com/ansible/latest/intro_installation.html)

3.[Docker](https://docs.docker.com/install/)

## Using Vagrant

### Launch app using Vagrant
To Launch VM box on Vagrant with the dockerized app using Ansible simply,
```bash
[local] vagrant up
```
After the deployment is finished, you could visit http://192.168.33.200 in a browser,


## Using Docker

### Launch app using Docker-Compose
To use the dockerized app simply,
```bash
[local] docker-compose up
```

### Launch interactive mode - troubleshooting

To launch interactive mode simply,
```bash
[local] $ docker-compose run --service-ports interactive
```

That will launch the simple-webapp with mongo container.
Then, inside the container, you can launch the app with

```bash
[interactive] $ npm start
```

To access the site, make sure you use the IP of the docker-machine

```bash
[local] $ open http://$(docker-machine ip $(docker-machine active))
```
